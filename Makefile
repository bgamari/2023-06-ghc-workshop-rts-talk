PANDOC_OPTS = --to=beamer --pdf-engine-opt=--shell-escape --filter=pandoc-step-filter --filter=svg-filter --citeproc --template=template.tex

INCLUDES = well-typed-theme/includes

all : talk.pdf

talk.pdf : talk.mkd
	TEXINPUTS=$(INCLUDES):$$TEXINPUTS pandoc ${PANDOC_OPTS} -o $@ $<

talk.tex : talk.mkd
	pandoc ${PANDOC_OPTS} --standalone -o $@ $<

