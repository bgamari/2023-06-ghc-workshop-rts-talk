{-# LANGUAGE MagicHash #-}

module ThunkBlock where

import qualified GHC.Vis as Vis
import Control.Concurrent (threadDelay, forkIO, myThreadId)
import Unsafe.Coerce (unsafeCoerce#)
import GHC.Conc (ThreadId(..))
import Control.Monad (void)
import System.IO.Unsafe (unsafePerformIO)
import Data.Tuple (Solo(..))

threadDelaySec :: Int -> IO ()
threadDelaySec n = threadDelay (n*1000000)

after :: Int -> IO a -> IO a
after n k = threadDelaySec n >> k

labelTSO :: String -> IO ()
labelTSO lbl = do
    ThreadId tid <- myThreadId
    Vis.view (unsafeCoerce# tid :: ()) lbl

pause :: String -> IO ()
pause pos = do
    Vis.update
    putStr $ "paused("++pos++")..."
    void getLine

longThunk :: Int -> Solo Int
longThunk n = 
    let x = unsafePerformIO $ do
            after n $ pause "in thunk"
            return n
    in Solo x

force :: Solo a -> IO a
force (Solo x) = return $! x

noBlock :: IO ()
noBlock = do
    let !thnk = longThunk 5
    Vis.vis
    Vis.view thnk "thnk"
    labelTSO "main-thrd"
    pause "before eval"

    void $ force thnk
    pause "after eval"
    
block :: IO ()
block = do
    let thnk = longThunk 5
    Vis.vis
    Vis.view thnk "thnk"

    pause "before eval"

    forkIO $ do
        labelTSO "tso1"
        void $ force thnk

    threadDelaySec 1
    pause "after eval"

    forkIO $ after 2 $ do
        labelTSO "tso2"
        void $ force thnk

    forkIO $ after 3 Vis.update

    pause "finished"

