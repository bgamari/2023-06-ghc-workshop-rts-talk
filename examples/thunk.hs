{-# LANGUAGE UnboxedTuples #-}
{-# OPTIONS_GHC -dno-typeable-binds -fforce-recomp -O1 #-}

-- | Examine the Cmm of this module; note the
-- structure of the foo.x binding's entry code.
module Hi where

import Data.Tuple

fib :: Int -> Int
fib n = go 0 1 1 n
  where
    go !acc !x !y 0 = acc
    go acc x y i =
        let y' = x+y
         in go (acc+x) y y' (i-1)
{-# NOINLINE fib #-}

foo :: Int -> Solo Int
foo n = let x = fib n in Solo x

