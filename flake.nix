{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    svg-filter.url = "github:bgamari/svg-filter";
    pandoc-step-filter.url = "github:bgamari/pandoc-step-filter";
  };

  outputs = { self, ...}@inputs:
    inputs.flake-utils.lib.eachDefaultSystem (system:
      let 
        pkgs = inputs.nixpkgs.legacyPackages."${system}";
        tex = pkgs.texlive.combine {
          inherit (pkgs.texlive)
            scheme-medium xstring svg totpages environ trimspaces titlecaps ifnextok
            enumitem ifplatform preprint ncctools comment relsize
            transparent catchfile acmart newtx upquote
            libertinus libertinus-fonts libertinust1math libertinus-otf
            libertinus-type1 inconsolata opensans libertine;
        };
        make = target: 
          pkgs.stdenv.mkDerivation {
            name = "talk-${target}";
            nativeBuildInputs = with pkgs; [
              tex pandoc
              librsvg
              inkscape
              gnumake
              ghc
              inputs.svg-filter.packages."${system}".default
              inputs.pandoc-step-filter.packages."${system}".default
            ];
            src = ./.;
            preferLocalBuild = true;
            buildPhase = ''
              make ${target}
            '';
            installPhase = ''
              cp ${target} $out
            '';
          };
      in {
          packages.default = make "talk.pdf";
          packages.tex = make "talk.tex";

          devShells.build = pkgs.mkShell {
            inputsFrom = [ self.packages.${system}.default ];
          };

          devShells.present = pkgs.mkShell {
            packages =
              let
                hspkgs = pkgs.haskell.packages.ghc928.override {
                  ghc = pkgs.haskell.compiler.ghc928.overrideAttrs (oldAttrs: {
                    patches = oldAttrs.patches ++ [ ./patches/0001-ghc-heap-Support-for-BLOCKING_QUEUE-closures.patch ];
                  });
                };
                ghc = hspkgs.ghcWithPackages (ps: [ ps.ghc-vis ]);
              in [ ghc pkgs.gtk3 ];

            shellHook = ''
              XDG_DATA_DIRS="$XDG_DATA_DIRS:$GSETTINGS_SCHEMAS_PATH:${pkgs.gnome3.adwaita-icon-theme}/share"
              PS1="\[$(tput setaf 204)> \[$(tput sgr0)"
              clear
              echo "Hello World!"
            '';
          };
        }
    );
}

